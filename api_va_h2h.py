from pydantic import BaseModel
from fastapi import FastAPI, APIRouter, Depends, HTTPException, Security, Response, Request, BackgroundTasks, datastructures
from datetime import datetime, date, timedelta
import hashlib
from bson.objectid import ObjectId
import requests
import math
import time
import json
from motor.motor_asyncio import AsyncIOMotorClient
import functools
import jwt
from fastapi.responses import HTMLResponse

requests = requests.Session()
requests.request = functools.partial(requests.request, timeout=30)

ENVIRONMENT = 'productionxx'

if ENVIRONMENT == 'production':
    MGDB_CLIENT = AsyncIOMotorClient(host= "35.197.129.58",port=27017,username="root",password= "zCmqQ7r34Quj")
    MGDB_ERP = MGDB_CLIENT["db_new_erp_prod"]
else:
    MGDB_CLIENT = AsyncIOMotorClient(host= "10.8.0.1",port=27017,username="root",password= "solusi2526")
    MGDB_ERP = MGDB_CLIENT["db_new_erp_staging"]


app = FastAPI(openapi_url="/api_va_h2h/openapi.json", docs_url="/api_va_h2h/doc")

signature = ''

def validate(checksum,str1='',str2='',str3=''):
    global signature
    if checksum != f"{signature}{str1}{str2}{str3}": return False
    return 'OK'


@app.get("/health")
async def health():
    return {"status": "ok"}


@app.get("/get_va_by_nis")
async def get_va_by_nis(nis:str,checksum:str):
    if not validate(checksum,nis) : raise HTTPException(status_code=404, detail="error checksum")
    return {"data":"va"}


@app.get("/get_all_va")
async def get_all_va(nis:str,checksum:str):
    print()


@app.get("/get_mutasi_per_va")
async def get_mutasi_per_va(va:str,date:str,checksum:str):
    print()

@app.get("/get_mutasi_all_va")
async def get_mutasi_all_va(date:str,checksum:str):
    print()


